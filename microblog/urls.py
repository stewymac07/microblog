from django.conf.urls import patterns, include, url
from django.contrib import admin

# from . import views
from blog.views import BlogListView, HomepageView

import settings

admin.autodiscover()

urlpatterns = patterns('',
    url(r"^$", HomepageView.as_view(),  name="home"),
    url(r'^blog/', include('blog.urls', namespace='blog')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += patterns('',
        (r'^static(.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT }),

)
