from django.views.generic import ListView, DetailView, TemplateView

from models import Post


class HomepageView(TemplateView):
   template_name = 'index.html'


class PublishedPostsMixin(object):
    def get_queryset(self):
        # queryset = super(PublishedPostsMixin, self).get_queryset()
        # return queryset.filter(published=True)
        return self.model.objects.live()

class BlogListView(PublishedPostsMixin, ListView):
    model = Post


class PostDetailView(PublishedPostsMixin, DetailView):
    model = Post
