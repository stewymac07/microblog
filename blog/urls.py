from django.contrib.auth.decorators import login_required, permission_required
from django.conf.urls.defaults import *
from django.views.generic.dates import *
from django.views.generic import ListView
from django.views.generic.simple import direct_to_template

import views

urlpatterns = patterns('',
    url(r"^$", views.BlogListView.as_view(),  name="list"),
    url(r"^blog/(?P<slug>[\w-]+)/$", views.PostDetailView.as_view(), name="detail"),
)